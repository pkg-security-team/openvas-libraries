Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: gvm-libs
Source: https://github.com/greenbone/gvm-libs

Files: *
Copyright: 2002-2003 Michel Arboi and Renaud Deraison
           2002-2004 Tenable Network Security, Inc
           2009-2023 Greenbone AG
License: GPL-2+

Files: util/passwordbasedauthentication.*
Copyright: 2020-2021 Greenbone AG
License: GPL-3+

Files: debian/*
Copyright: 2016 ChangZhuo Chen (陳昌倬) <czchen@debian.org>
           2016-2019 SZ Lin (林上智) <szlin@debian.org>
           2020-2023 Sophie Brun <sophie@offensive-security.com>
License: GPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".
